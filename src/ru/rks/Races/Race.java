package ru.rks.Races;


public class Race {
    final static int COUNT_OF_STEPS = 5000;
    static final int TIME_OF_SLEEP = 0;

    public static void main(String[] args) {
        Thread thread = Thread.currentThread();
        Racer racer = new Racer();
        racer.setPriority(Thread.MAX_PRIORITY);
        thread.setPriority(Thread.MIN_PRIORITY);
        System.out.println(racer.getPriority());
        System.out.println(thread.getPriority());
        racer.start();
        for (int i = 0; i < COUNT_OF_STEPS; i++) {
            if (i == 1000) {
                racer.setPriority(Thread.MIN_PRIORITY);
                thread.setPriority(Thread.MAX_PRIORITY);
            }
            try {
                Thread.sleep(TIME_OF_SLEEP);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(i);
        }
    }
}

